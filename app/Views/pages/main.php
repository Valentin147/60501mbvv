<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <main class="text-center">
    <div class="jumbotron">
        <img class="mb-4" src="https://www.flaticon.com/svg/static/icons/svg/2490/2490332.svg" alt="" width="72" height="72"><h1 class="display-4">Countries</h1>
        <p class="lead">Это приложение предоставит основные географические и экономические сведения о государствах и территориях.</p>
        <a class="btn btn-primary btn-lg" href="#" role="button">Войти</a>
        <a href="<?= $authUrl; ?>" class="btn btn-lg btn-outline-dark" role="button" style="text-transform:none">
            <img width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
            <?= lang('Войти с Google') ?>
            </a>
    </div>
</main>
<?= $this->endSection() ?>