<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($country) && is_array($country)) : ?>
            <h2>Все государства:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?= form_open('country/viewAllWithCurrency', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                </form>
                <?= form_open('country/viewAllWithCurrency',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Название или Столица" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                </form>
            </div>
            <table class="table table-striped">
                <thead>
                <th scope="col">Флаг</th>
                <th scope="col">Название</th>
                <th scope="col">Валюта</th>
                <th scope="col">Население,млн</th>
                <th scope="col">Столица</th>
                <th scope="col">Площадь,км<sup>2</sup></th>
                <th scope="col">Управление</th>

                </thead>
                <tbody>
                <?php foreach ($country as $item): ?>
                    <tr>
                        <td>
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img src="https://borzilova.ru/wp-content/uploads/2018/06/%D0%B3%D0%BE%D1%81%D1%83%D0%B4.jpg" class="card-img" alt="<?= esc($item['id']); ?>">
                            <?php else:?>
                                <img src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['id']); ?>">
                            <?php endif ?>
                        </td>
                        <td><?= esc($item['country_title']); ?></td>
                        <td><?= esc($item['Currency']); ?></td>
                        <td><?= esc($item['MPopulation']); ?></td>
                        <td><?= esc($item['Capital']); ?></td>
                        <td><?= esc($item['Area']); ?></td>
                        <td>
                            <a href="<?= base_url()?>/country/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                            <a href="<?= base_url()?>/country/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/country/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">

                <p>Страны не найдены </p>
                <a class="btn btn-primary btn-lg" href="<?= base_url()?>/country/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Добавить государство </a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>