<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2>Все государства</h2>

<?php if (!empty($country) && is_array($country)) : ?>

    <?php foreach ($country as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                 <?php if (is_null($item['picture_url'])) : ?>
                    <img src="https://borzilova.ru/wp-content/uploads/2018/06/%D0%B3%D0%BE%D1%81%D1%83%D0%B4.jpg" class="card-img" alt="<?= esc($item['id']); ?>">
                <?php else:?>
                    <img src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['id']); ?>">
                 <?php endif ?>
                </div>
                <div class="col-md-8">

                    <div class="card-body">
                        Название:<b><p class="card-text"><?= esc($item['country_title']); ?></p></b>
                        Столица: <b><p class="card-text"><?= esc($item['Capital']); ?></p></b>
                        <a href="<?= base_url()?>/index.php/country/view/<?= esc($item['id']); ?>" class="btn btn-primary">Подробнее о государстве</a>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
<?php else : ?>
        <p>Невозможно найти страны</p>
<?php endif ?>
</div>
<?= $this->endSection() ?>