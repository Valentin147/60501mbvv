<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('country/store'); ?>
        <div class="form-group">
            <label for="Country_Title">Название</label>
            <input type="text" class="form-control <?= ($validation->hasError('country_title')) ? 'is-invalid' : ''; ?>" name="country_title"
                   value="<?= old('country_title'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('country_title') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="Capital">Столица</label>
            <input type="text" class="form-control <?= ($validation->hasError('Capital')) ? 'is-invalid' : ''; ?>" name="Capital"
                   value="<?= old('Capital'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Capital') ?>
            </div>

        </div>

        <div class="form-group">
            <label for="MPopulation">Население, млн человек</label>
            <input type="number" size="10" min="0" max="2000" class="form-control <?= ($validation->hasError('MPopulation')) ? 'is-invalid' : ''; ?>" name="MPopulation"
                   value="<?= old('MPopulation'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('MPopulation') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="Area">Площадь, км<sup>2</sup></label>
            <input type="number" size="10" class="form-control <?= ($validation->hasError('Area')) ? 'is-invalid' : ''; ?>" name="Area"
                   value="<?= old('Area'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Area') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="Religion">Основная религия</label>
            <input type="text" class="form-control <?= ($validation->hasError('Religion')) ? 'is-invalid' : ''; ?>" name="Religion"
                   value="<?= old('Religion'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Religion') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="picture">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Добавить</button>
        </div>
        </form>

    </div>
<?= $this->endSection() ?>