<?php
namespace App\Models;
use CodeIgniter\Model;
class YcirculModel extends Model
{
    protected $table = 'ycircul'; //таблица,связанная с моделью
    protected $allowedFields = ['idcountry1', 'idcountry2', 'exportfrom_1to2', 'CYear'];
}
