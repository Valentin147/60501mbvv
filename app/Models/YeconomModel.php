<?php namespace App\Models;
use CodeIgniter\Model;
class YeconomModel extends Model
{
    protected $table = 'yeconom'; //таблица, связанная с моделью
    protected $allowedFields = ['ID', 'GDP', 'PCI', 'YYear', 'Currency'];


    public function getYeconom($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}