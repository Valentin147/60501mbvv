<?php namespace App\Models;
use CodeIgniter\Model;

class CountryModel extends Model
{
    protected $table = 'country'; //таблица, связанная с моделью
    protected $allowedFields = ['country_title', 'Capital', 'MPopulation', 'Area', 'Religion', 'picture_url'];

    public function getCountry($id = null)
    {
    if (!isset($id)) {
            return $this->findAll();

        }
        return $this->where(['id' => $id])->first();
    }

    public function getCountryWithCurrency($id = null, $search = '')
    {
        if ($search != '') {
            $search = strtolower($search);
            $search = ucfirst($search);
        }
        $builder = $this
            ->select('*')
            ->join('yeconom','yeconom.ID = country.id')
            ->like('country_title', $search,'both', null, false)
            ->orlike('Capital',$search,'both',null, false);

        if (!is_null($id))
        {
            return $builder->where(['country.id' => $id])->first();
        }
        return $builder;
    }


      public function getCountryCircul($id = null)
     {
        $builder1 = $this->db->query('select t.idcountry2, sum(t.exportfrom_1to2) 
                                from ((select idcountry1, idcountry2, exportfrom_1to2 from ycircul where idcountry1 = '.$id.')
                                union (select idcountry2, idcountry1, -exportfrom_1to2 from ycircul where idcountry2 = '.$id.')) as t 
                                group by idcountry2');
  /*       if (!is_null($id))
         {
             return $builder1->where(['country.id' => $id])->first();
         }*/
         return $builder1;
     }
}