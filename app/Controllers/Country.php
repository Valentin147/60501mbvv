<?php namespace App\Controllers;
use App\Models\CountryModel;
use Aws\S3\S3Client;
//use CodeIgniter\Controller;

class Country extends BaseController
{
    public function index() //Отображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
    $model = new CountryModel();
    $data['country'] = $model->getCountry();
        //echo view('templates/header', $data);
          echo view('country/view_all', $this->withIon($data));
       //echo view('templates/footer', );
     }
    public function viewAllWithCurrency()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new CountryModel();
            $data['country'] = $model->getCountryWithCurrency(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('country/view_all_with_currency', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Для просмотра этой таблицы нужно обладать правами администратора'));
            return redirect()->to('/auth/login');
        }
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
                }
        $model = new CountryModel();
        $data['country'] = $model->getCountry($id);
        $data['builder1'] = $model->getCountryCircul($id);
        var_dump($data['builder1']);
         //echo view('templates/header', $data);
        echo view('country/view', $this->withIon($data));
        //echo view('templates/footer', $data);
        die();

}
    public function create()
    {
       if (!$this->ionAuth->loggedIn())
    {
        return redirect()->to('/auth/login');
    }
       helper(['form']);
       $data ['validation'] = \Config\Services::validation();
       echo view('country/create', $this->withIon($data));
    }
    public function store()
   {
       helper(['form','url']);
           if ($this->request->getMethod() === 'post' && $this->validate([
                        'country_title' => 'required',
                        'Capital'  => 'required',
                        'MPopulation'  => 'required',
                        'Area'  => 'required',
                        'Religion' => 'required',
                        'picture'  => 'is_image[picture]|max_size[picture,1024]',
                   ]))
                   {
                       $insert = null;
                       //получение загруженного файла из HTTP-запроса
                       $file = $this->request->getFile('picture');
                       if ($file->getSize() != 0) {
                           //подключение хранилища
                           $s3 = new S3Client([
                               'version' => 'latest',
                               'region' => 'us-east-1',
                               'endpoint' => getenv('S3_ENDPOINT'),
                               'use_path_style_endpoint' => true,
                               'credentials' => [
                                   'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                                   'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                               ],
                           ]);
                           //получение расширения имени загруженного файла
                           $ext = explode('.', $file->getName());
                           $ext = $ext[count($ext) - 1];
                           //загрузка файла в хранилище
                           $insert = $s3->putObject([
                               'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                               //генерация случайного имени файла
                               'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                               'Body' => fopen($file->getRealPath(), 'r+')
                           ]);

                       }
             $model = new CountryModel();
                       $data = [
                           'country_title' => $this->request->getPost('Country_Title'),
                           'Capital' => $this->request->getPost('Capital'),
                           'MPopulation' => $this->request->getPost('MPopulation'),
                           'Area' => $this->request->getPost('Area'),
                           'Religion' => $this->request->getPost('Religion'),
                       ];
                       if (!is_null($insert))
                           $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
                   //session()->setFlashdata('message', 'This Is a Message');
                   return redirect()->to('/country');

         }
        else
            {
                return redirect()->to('/country/create')->withInput();
            }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new CountryModel();

        helper(['form']);
        $data ['country'] = $model->getCountry($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('country/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/country/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'country_title' => 'required',
                'Capital' => 'required',
                'MPopulation' => 'required',
                'Area' => 'required',
                'Religion' => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new CountryModel();
            $data = [
                'country_title' => $this->request->getPost('country_title'),
                'Capital' => $this->request->getPost('Capital'),
                'MPopulation' => $this->request->getPost('MPopulation'),
                'Area' => $this->request->getPost('Area'),
                'Religion' => $this->request->getPost('Religion'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->update($this->request->getPost('id'), $data);
            //session()->setFlashdata('message', 'This Is a Message');
                    return redirect()->to('/country');
      }
        else
        {
           return redirect()->to('/country/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new CountryModel();
        $model->delete($id);
        return redirect()->to('/country');
     }
 }