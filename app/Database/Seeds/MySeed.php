<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MySeed extends Seeder
{
    public function run()
    {
        $data = [

            'Country_Title' => 'Russia',
            'Capital'=>'Moscow',
            'MPopulation' => 144,
            'Area' => 17000000,
            'Religion' => 'Orthodoxy',
        ];
        $this->db->table('country')->insert($data);

        $data = [
            'Country_Title' => 'USA',
            'Capital' => 'Washington',
            'MPopulation' => 327,
            'Area' => 9600000,
            'Religion' => 'Catholicism',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'China',
            'Capital' => 'Beijing',
            'MPopulation' => 1390,
            'Area' => 9600000,
            'Religion' => 'Atheism',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'Norway',
            'Capital' => 'Oslo',
            'MPopulation' => 5,
            'Area' => 400000,
            'Religion' => 'Lutheranism',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'Italy',
            'Capital' => 'Rome',
            'MPopulation' => 61,
            'Area' => 300000,
            'Religion' => 'Catholicism',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'France',
            'Capital' => 'Paris',
            'MPopulation' => 67,
            'Area' => 600000,
            'Religion' => 'Catholicism',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'Chad',
            'Capital' => 'N`Djamena',
            'MPopulation' => 15,
            'Area' => 1300000,
            'Religion' => 'Islam',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'Ecuador',
            'Capital' => 'Quito',
            'MPopulation' => 17,
            'Area' => 300000,
            'Religion' => 'Christianity',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'Japan',
            'Capital' => 'Tokyo',
            'MPopulation' => 127,
            'Area' => 400000,
            'Religion' => 'Buddhism',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'Country_Title' => 'Egypt',
            'Capital' => 'Cairo',
            'MPopulation' => 98,
            'Area' => 1000000,
            'Religion' => 'Islam',
        ];
        $this->db->table('country')->insert($data);

        $data = [

            'ID' => 1,
            'GDP' => 1600000,
            'PCI' => 11327,
            'YYear'=> 2019,
            'Currency' => 'Ruble',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 2,
            'GDP' => 20500000,
            'PCI' => 62606,
            'YYear'=> 2017,
            'Currency' => 'U.S. Dollar',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [
            'ID' => 3,
            'GDP' => 13600000,
            'PCI' => 9608,
            'YYear'=> 2014,
            'Currency' => 'Yu',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 4,
            'GDP' => 450000,
            'PCI' => 76684,
            'YYear'=> 2018,
            'Currency' => 'Euro',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 5,
            'GDP' => 2100000,
            'PCI' => 34260,
            'YYear'=> 2018,
            'Currency' => 'Euro',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 6,
            'GDP' => 3000000,
            'PCI' => 42878,
            'YYear'=> 2011,
            'Currency' => 'Euro',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 7,
            'GDP' => 11300,
            'PCI' => 874,
            'YYear'=> 2016,
            'Currency' => 'C.A. Franc',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 8,
            'GDP' => 108000,
            'PCI' => 6316,
            'YYear'=> 2015,
            'Currency' => 'U.S. Dollar',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 9,
            'GDP' => 5000000,
            'PCI' => 39306,
            'YYear'=> 2015,
            'Currency' => 'Yen',
        ];
        $this->db->table('yeconom')->insert($data);

        $data = [

            'ID' => 10,
            'GDP' => 251000,
            'PCI' => 2573,
            'YYear'=> 2009,
            'Currency' => 'Eg. pound',
        ];
        $this->db->table('yeconom')->insert($data);
    }
}
