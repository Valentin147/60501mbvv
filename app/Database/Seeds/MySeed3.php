<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MySeed3 extends Seeder
{
    public function run()
    {
        $data = [

            'IDCountry1' => 1,
            'IDCountry2' => 10,
            'ExportFrom_1To2' => 26,
            'CYear' => 2020,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 2,
            'IDCountry2' => 9,
            'ExportFrom_1To2' => 42,
            'CYear' => 2018,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 3,
            'IDCountry2' => 8,
            'ExportFrom_1To2' => 54,
            'CYear' => 2016,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 4,
            'IDCountry2' => 7,
            'ExportFrom_1To2' => 11,
            'CYear' => 2014,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 5,
            'IDCountry2' => 6,
            'ExportFrom_1To2' => 24,
            'CYear' => 2015,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 1,
            'IDCountry2' => 10,
            'ExportFrom_1To2' => 31,
            'CYear' => 2017,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 8,
            'IDCountry2' => 5,
            'ExportFrom_1To2' => 10,
            'CYear' => 2019,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 7,
            'IDCountry2' => 6,
            'ExportFrom_1To2' => 11,
            'CYear' => 2013,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 1,
            'IDCountry2' => 2,
            'ExportFrom_1To2' => 111,
            'CYear' => 2015,
        ];
        $this->db->table('circul')->insert($data);

        $data = [

            'IDCountry1' => 4,
            'IDCountry2' => 3,
            'ExportFrom_1To2' => 22,
            'CYear' => 2016,
        ];
        $this->db->table('circul')->insert($data);
    }
}
