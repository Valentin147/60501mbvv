<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Circul extends Migration
{
    public function up()
    {
        // country
        if (!$this->db->tableexists('circul')) {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'IDCountry1' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'IDCountry2' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'ExportFrom_1To2' => array('type' => 'INT', 'null' => FALSE),
                'CYear' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => TRUE),
            ));
            $this->forge->addForeignKey('IDCountry1','country','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('IDCountry2','country','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('circul', TRUE);
        }
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('circul');
    }
}
