<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Count extends Migration
{
    public function up()
    {
        // country
        if (!$this->db->tableexists('country'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Country_Title' => array('type' => 'VARCHAR', 'constraint' => '80', 'null' => TRUE),
                'Capital' => array('type' => 'VARCHAR', 'constraint' => '80', 'null' => TRUE),
                'MPopulation' => array('type' => 'INT', 'null' => TRUE),
                'Area' => array('type' => 'INT', 'null' => TRUE),
                'Religion' => array('type' => 'VARCHAR', 'constraint' => '80', 'null' => TRUE),
            ));
            // create table
            $this->forge->createtable('country', TRUE);
        }

        if (!$this->db->tableexists('yeconom'))
        {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'GDP' => array('type' => 'INT', 'null' => TRUE),
                'PCI' => array('type' => 'INT', 'null' => TRUE),
                'YYear' => array('type' => 'INT', 'null' => TRUE),
                'Currency' => array('type' => 'VARCHAR', 'constraint' => '80', 'null' => TRUE),
            ));

            $this->forge->addForeignKey('ID','country','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('yeconom', TRUE);
        }
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('yeconom');
        $this->forge->droptable('country');
    }
}
