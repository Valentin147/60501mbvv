-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Дек 15 2020 г., 17:42
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Macroeconomics`
--

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE `country` (
  `id` int UNSIGNED NOT NULL,
  `Country_Title` varchar(80) DEFAULT NULL,
  `Capital` varchar(80) DEFAULT NULL,
  `MPopulation` int DEFAULT NULL,
  `Area` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `Country_Title`, `Capital`, `MPopulation`, `Area`) VALUES
(1, 'Россия', 'Москва', 144, 17000000),
(2, 'США', 'Вашингтон', 327, 9600000),
(3, 'Китай', 'Пекин', 1390, 9600000),
(4, 'Норвегия', 'Осло', 5, 400000),
(5, 'Италия', 'Рим', 61, 300000),
(6, 'Франция', 'Париж', 67, 600000),
(7, 'Чад', 'Нджамена', 15, 1300000),
(8, 'Эквадор', 'Кито', 17, 300000),
(9, 'Япония', 'Токио', 127, 400000),
(10, 'Египет', 'Каир', 98, 1000000);

-- --------------------------------------------------------

--
-- Структура таблицы `event`
--

CREATE TABLE `event` (
  `Event_ID` int UNSIGNED NOT NULL,
  `id` int UNSIGNED DEFAULT NULL,
  `Event_Name` varchar(30) DEFAULT NULL,
  `Event_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `event`
--

INSERT INTO `event` (`Event_ID`, `id`, `Event_Name`, `Event_Date`) VALUES
(1, 1, 'Единый день голосования', '2019-09-08'),
(2, 2, 'Запуск спутников Wi-Fi', '2019-11-12'),
(3, 4, 'День рождения Короля', '2019-02-21'),
(4, 5, 'Карнавал Венеция', '2019-02-16'),
(5, 7, 'Закрытие границы с Ливией', '2019-03-04'),
(6, 7, 'Сход оползня', '2019-04-26'),
(7, 6, 'Женский ЧМ по футболу', '2019-06-07'),
(8, 5, 'Фестивать артишоков', '2019-04-19'),
(9, 10, 'Восстановление турпотока', '2019-08-31'),
(10, 8, 'Массовые протесты', '2019-10-03'),
(11, 9, 'Мощное землетрясение', '2019-08-04'),
(12, 3, 'Вспышка коронавируса', '2019-11-30');

-- --------------------------------------------------------

--
-- Структура таблицы `ycircul`
--

CREATE TABLE `ycircul` (
  `Country1_ID` int UNSIGNED DEFAULT NULL,
  `Country2_ID` int UNSIGNED DEFAULT NULL,
  `Export` int UNSIGNED DEFAULT NULL,
  `Import` int UNSIGNED DEFAULT NULL,
  `Saldo` int UNSIGNED DEFAULT NULL,
  `CYear` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `ycircul`
--

INSERT INTO `ycircul` (`Country1_ID`, `Country2_ID`, `Export`, `Import`, `Saldo`, `CYear`) VALUES
(2, 1, 35, 40, 5, 2014),
(3, 4, 10, 1, 9, 2011),
(3, 6, 14, 9, 5, 2017),
(4, 2, 2, 12, 10, 2016),
(6, 7, 6, 4, 2, 2012),
(8, 10, 9, 11, 2, 2013),
(10, 1, 14, 12, 2, 2015),
(5, 9, 10, 16, 6, 2018),
(9, 5, 16, 10, 6, 2018),
(3, 1, 50, 61, 11, 2019);

-- --------------------------------------------------------

--
-- Структура таблицы `yeconom`
--

CREATE TABLE `yeconom` (
  `id` int UNSIGNED DEFAULT NULL,
  `YYear` int UNSIGNED DEFAULT NULL,
  `GDP` int UNSIGNED DEFAULT NULL,
  `PCI` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `yeconom`
--

INSERT INTO `yeconom` (`id`, `YYear`, `GDP`, `PCI`) VALUES
(1, 2019, 1600000, 11327),
(2, 2017, 20500000, 62606),
(3, 2014, 13600000, 9608),
(4, 2018, 450000, 76684),
(5, 2018, 2100000, 34260),
(6, 2011, 3000000, 42878),
(7, 2016, 11300, 874),
(8, 2015, 108000, 6316),
(9, 2015, 5000000, 39306),
(10, 2009, 251000, 2573);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`Event_ID`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `ycircul`
--
ALTER TABLE `ycircul`
  ADD KEY `Country1_ID` (`Country1_ID`),
  ADD KEY `Country2_ID` (`Country2_ID`);

--
-- Индексы таблицы `yeconom`
--
ALTER TABLE `yeconom`
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `country`
--
ALTER TABLE `country`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `event`
--
ALTER TABLE `event`
  MODIFY `Event_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ограничения внешнего ключа сохраненных таблиц
--
--
-- Ограничения внешнего ключа таблицы `event`
--

--
-- Ограничения внешнего ключа таблицы `ycircul`
--
ALTER TABLE `country`
  ADD CONSTRAINT `ycircul_ibfk_1` FOREIGN KEY (`Country1_ID`) REFERENCES `ycircul` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `ycircul_ibfk_2` FOREIGN KEY (`Country2_ID`) REFERENCES `ycircul` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `yeconom`
--
ALTER TABLE `yeconom`
  ADD CONSTRAINT `yeconom_ibfk_1` FOREIGN KEY (`id`) REFERENCES `country` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
